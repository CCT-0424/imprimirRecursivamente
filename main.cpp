#include <iostream>

using namespace std;

const int tamanho = 3;

int vetor[tamanho];

void lerVetor(int vetor[], int tamanho){
    for (int i=0; i < tamanho; i++){
        cout << "Vetor["<< (i+1) << "]: ";
        cin >> vetor[i];
    }
}

void imprimir (int vetor[],int inicio, int fim){
    if (inicio == fim){
        cout << "Vetor["<< (inicio+1) << "]: " << vetor[inicio] << endl;
    }
}

void imprimirRecursivamente(int vetor[],int inicio, int fim){

    if (inicio < fim){
        int meio = (inicio+fim)/2;
        imprimirRecursivamente(vetor,inicio,meio);
        imprimirRecursivamente(vetor,meio+1,fim);
    }
    imprimir(vetor,inicio,fim);
}

int main()
{
    lerVetor(vetor,tamanho);
    imprimirRecursivamente(vetor,0,tamanho-1);
    cout << "Hello world!" << endl;
    return 0;
}
